<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarRelProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * * La taula segons artisan era 'car_rel_products', pero ja qu es una de relacions many to many, la deixarem en singular car_rel_product. El Model segueix com tots en sigular.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_rel_product', function (Blueprint $table) {
            $table->unsignedInteger('car_id');
            $table->integer('product_id')->unsigned(); //Hauria de donar el mateix tipus que car_id
            $table->timestamps();
            $table->foreign('car_id')
                ->references('id')->on('cars')->onDelete('cascade');
            $table->foreign('product_id')
                ->references('id')->on('car_products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_rel_product');
    }
}
