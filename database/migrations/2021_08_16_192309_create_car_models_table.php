<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarModelsTable extends Migration
{
    /**
     * Run the migrations. Relacio One-To-Many amb taula cars.
     *
     * @return void
     */
    public function up()
    {
        schema::create('car_models', function (Blueprint $table) { //one to many
            $table->increments("id");
            $table->unsignedInteger("car_id");
            $table->string("model_name");
            $table->longtext("description");
            $table->timestamps();
            $table->foreign('car_id')
                ->references('id')->on('cars')
                //->onDelete('set null') //Això deixaria l'id en NULL, però no borraria el registre
                ->onDelete('cascade'); //Això si que borra el registre
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_models');
    }
}
