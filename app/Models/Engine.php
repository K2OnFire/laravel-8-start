<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Engine extends Model
{
    use HasFactory;

    /**
     * Reverse relationship: A Engine belongs to a carModel
     */
    public function carModel(){
        return $this->belongsTo(CarModel::class);
    }
}
