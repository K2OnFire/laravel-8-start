<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    use HasFactory;

    protected $table = 'car_models'; //opcional, No es necessari si el nom ja es el matex en plural
    protected $primaryKey = 'id'; //opcional, no es necessari si utilitzem el primary del autoincrmenet original.

    /**
     * Reverse relationship: A car model belongs to a car
     */
    public function car(){
        return $this->belongsTo(Car::class);
    }
}
