<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * private model for extra security (captcha) in forms
 */
class K2captcha extends Model
{
    public $question;
    public $title;
    public $type;
    public $inputName; //L'input pot variar, aixi els robots els hi costa mes saber on llegir
    public $realValue;
    public $fakeValue;
    public $availableOptions;
}
