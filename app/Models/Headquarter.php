<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Headquarter extends Model
{
    use HasFactory;

    /**
     * Reverse relationship: A headquarter belongs to a car
     * DEFINIR aquesta RELACIÓ INVERSA si volem trobar el headquarter sense el car
     */
    public function car(){
        return $this->belongsTo(Car::class);
    }
}
