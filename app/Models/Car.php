<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

//php artisan make:model Car -m
//  amb -m crea el migration tb

class Car extends Model
{
    use HasFactory;

    protected $table = 'cars'; //opcional, No es necessari si el nom ja es el matex en plural
    protected $primaryKey = 'id'; //opcional, no es necessari si utilitzem el primary del autoincrmenet original.
    //public $timestamps = true; //opcional, Si es fan servir l'updated_at i l'insert_at, default= true
    //protected $dateFormat = 'H:i:s'; //opcional, Format per defecte

    protected $fillable = ['name', 'founded', 'description', 'image_path', 'user_id']; //necessari si en el controllador utilitzem: $car = Car::create([]); per crear un registre. són les columnes que guardarà

    protected $hidden = ['password', 'remember_token']; //Els oculta en sortides de coleccions d'objects, arrays i json

    //protected $visible = ['name','founded','description'];// Si activem aquesta opcio, s'oculta tot i només es mostra les columnes especificades aqui.

    /**
     * Relacio One-To-Many (nom en plural)
     */
    public function carModels()
    {
        return $this->hasMany(CarModel::class);
    }

    /**
     * Relacio One-To-One (nom en singular)
     */
    public function headquarter()
    {
        return $this->hasOne(Headquarter::class);
    }

    /**
     * Relacio Has-Many (util en una relacio [MANY:Engine->CarModel] d'una relacio [ONE:CarModel->Car])
     */
    public function engines()
    {
        return $this->hasManyThrough(
            Engine::class,  //model final,
            CarModel::class, //model origen
            'car_id', //Foregn Key on CarModel table
            'model_id' //Foreign key on engine table
        );
    }

    /**
     * Relacio Has-One
     * Util en una relacio [ONE:CarProductionDate->CarModel] d'una relacio [ONE:CarModel->Car]
     * *Agafem un registre segons Car (fk car_id), no es segons CarModel (fk id). pertant nomes en volem un (el 1r)
     */
    public function productionDate()
    {
        return $this->hasOneThrough(
            CarProductionDate::class,  //model final,
            CarModel::class, //model origen
            'car_id', //Foregn Key on CarModel table
            'model_id' //Foreign key on CarProductionDate table
        );
    }

    public function products()
    {
        return $this->belongsToMany(CarProduct::class, 'car_rel_product', 'car_id', 'product_id');
    }
}
