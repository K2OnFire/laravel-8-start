<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Car;
use App\Http\Controllers\K2captchaController;
use App\Http\Requests\CreateCarValidationRequest;
use Illuminate\Validation\Rule;
use App\Models\CarProduct;
use App\Models\Headquarter; //nomes si volem trobar el headquarter sense el car
use Illuminate\Support\Facades\DB; //per la paginacio DB::table('cars')->paginate(4);

//php artisan make:controller CarsController --resource
//  amb --resource crea les funcions indez, create, store, show, update, destroy

class CarsController extends Controller
{

    public function __construct()
    {
        //Nomes deixem pùblics els endpoints index i show, la resta van amb user autentificat
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$cars = Car::where('name', '=', 'Audi')
        //->get();
        //->firstOrFail();

        $cars = Car::all(); //Si volem treballar amb objects / array d'obects, és ok (collections)
        //$cars = Car::all()->toArray(); //Es un array, va bé

        //$cars = Car::all()->toJson(); //Es un JSON, també va bé, tot i que normalment està reservat per APIs
        //$cars = json_decode($cars); //Ara el tenim com a Object i el podem itrerar en foreach com all();

        //print_r(Car::all()->count());

        /*//Si hi ha molts valors, per no saturar la memoria, això faria multiples querys de 2 rows
        $cars = Car::chunk(2, function($cars){
            foreach ($cars as $car){
                //print_r($car);
                dd($car);
            }
        });*/
        //dd($cars);

        //paginació amb QueryBuilder:
        //$cars = DB::table('cars')->paginate(4);

        //paginació amb eloquent (millor)
        $cars = Car::paginate(3); //Sobreescriu ->all();



        return view('cars/index', ['cars' => $cars]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //K2captchaController::clearSession();//Treure aquesta linia un cop testejat
        $captcha = K2captchaController::init();
        //dd($captcha);
        return view('cars/create', [
            'captcha' => $captcha
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //public function store(Request $request) //Original
    public function store(CreateCarValidationRequest $request)
    {

        /*
        //Metode -per guardar- 1
        $car = new Car;
        $car->name=$request->input("name");
        $car->founded=$request->input("founded");
        $car->description=$request->input("description");
        K2captchaController::clearSession();
        //dd($request);
        //Comprovar el captcha abans d'enviar
        $car->save();
        */

        //Request all input fields
        $test = $request->all(); //Array amb tot

        //Except method
        //$test = $request->except('_token'); //Se li pot passar el nom del imput o els noms amb array a excloure

        //Only method
        //$test = $request->only(['name','founded']); //Se li pot passar el nom del imput o els noms amb array a incloure

        //Comprovar que un camp s'hagi entrat
        //$test = $request->has('founded'); //boolean, per posteriors ifs

        //Comprovar d'on bé
        //$test = $request->path(); //"cars"
        if ($request->is("cars") && $request->isMethod("post")) {
            //Es la ruta original, guardem el form
            //Validem les dades (form vaidation)
            //dd($test);
            /*
            $captcha = K2captchaController::fillObjectFromSession();
            $request->validate([
                'name' => 'required|unique:cars',
                'founded' => 'required|integer|min:0|max:' . date('Y') . '',
                //'description' => 'optional',
            ]); //Si algun no passsa el control redirecciona a la pagina del form amb els errors.
            */
            //$test = $request->file("image")->guessExtension();
            /* guessExtension()
                getMimeType()
                store()
                asStore()
                storePublicly()
                move()
                getClientOriginalName()
                */
            //dd($test);
            //dd($request->all());
            $captcha = K2captchaController::fillObjectFromSession();
            $request->validated(); //Validacions generals programades en CreateCarValidationRequest
            $request->validate([ //Validacions extres especifiques d'aquest action
                $captcha->inputName => [
                    'required',
                    Rule::in([$captcha->realValue, 'pigscanfly'])
                ],
                'image' => 'mimes:jpg,png,jpeg|max:5048'
            ]); // Tal com ha quedat ara, fa 2 pasos de validacions, si peta el validated() i el validate(), nomes mostrarà els errors del validated() fins que s'hagin acabat i despres mostrarà els altres

            //carreguem l'imatge
            if ($request->has('image')) {
                $newImageName = time() . '-' . $request->path() . "-" . strtolower($request->name) . "." . $request->image->extension();
                $request->image->move(storage_path('app/public/cars/images'), $newImageName);
            } else {
                $newImageName = "";
            }
            //Metode -per guardar- 2 (mes optim)
            $car = Car::create([
                'name' => $request->input("name"),
                'founded' => $request->input("founded"),
                'description' => $request->input("description"),
                'image_path' => $newImageName,
                'user_id' => auth()->user()->id
                //El captcha no cal, pero s'ha de comprovar abans d'aquesta comanda, perquè això ja grava;
            ]);
        } else {
            //Fem sortir error
        }

        //$test = $request->url(); //tota la URL sencera
        //$test = $request->ip();
        //dd($test);




        return redirect("/cars");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //dd($id);
        $car = Car::find($id);
        //$hq= $car->headquarter; //per fer dd($hq) i comprovar el resultat
        //dd($hq);
        //$hqInverted = Headquarter::find($id); //nomes si volem trobar el headquarter sense el car
        //var_dump($car->products);
        //dd($car->productionDate);
        //$products = CarProduct::find($id); //relacio inversa
        //print_r($products);
        return view('cars/show')->with('car', $car);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$car = Car::where('id',$id)->first(); //Aaixò funciona bé, però no és lo més optim, si no existeix retorna buit.
        $car = Car::find($id); //Això fa el where per la clau principal + limit 1, si no exsteix retirna null
        //dd($car);
        $captcha = K2captchaController::init();
        return view('cars/edit')->with('car', $car)->with('captcha', $captcha);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Metode 2 (mes optim)
        $car = Car::where('id', $id)->update([
            'name' => $request->input("name"),
            'founded' => $request->input("founded"),
            'description' => $request->input("description"),
            //El captcha no cal, pero s'ha de comprovar abans d'aquesta comanda, perqu+e això ja grava;
        ]);

        return redirect("/cars");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        //dd($_POST);
        $car->delete();
        return redirect("/cars");
    }
    /**
     * Aquesta es la funcio original, funciona, po recomanen el canvi per la de dalt
     */
    public function destroyOriginal($id)
    {
        //dd($_POST);
        $car = Car::find($id);
        $car->delete();
        return redirect("/cars");
    }
}
