<?php

namespace App\Http\Controllers;

use App\Models\K2captcha;

/**
 * private controller for extra security (captcha) in forms
 */
class K2captchaController extends Controller
{
    /**
     * Crea la sessio amb captcha
     * Si ja existeix i no s'ha comprovat no fa res mes
     * Si es creen nous tipus de captcha (en methods) caldrà actualitzar $availableCaptchas i el switch
     *
     * @return App\Models\K2captcha;
     */
    public static function init()
    {
        $availableCaptchas = ["createWithNumbers", "createWithColors"];
        if (session('K2captcha.realValue') !== null) {
            $c = self::fillObjectFromSession();
        } else {
            $captchaIndex = rand(0, count($availableCaptchas) - 1);
            $captcha = $availableCaptchas[$captchaIndex];
            switch ($captcha) {
                case 'createWithNumbers':
                    $c = self::createWithNumbers(1, 10);
                    break;
                case 'createWithColors':
                    $c = self::createWithColors();
                    break;
                default:
                    $c = self::createWithNumbers(1, 10);
            }
        }
        return $c;
    }

    public static function createWithNumbers($min, $max)
    {
        session(['K2captcha.type' => __FUNCTION__]);

        //Base
        $arrParams["min"] = $min;
        $arrParams["max"] = $max;
        $arrParams["operations"] = ["+", "-"];
        $arrParams["operationChoice"]  = rand(0, count($arrParams["operations"]) - 1);
        $arrParams["base"] = rand(0, 20);
        $arrParams["toOp"] = rand($arrParams["min"], $arrParams["max"]);

        //Question
        $arrParams = self::createWithNumbersQuestion($arrParams);

        //title
        session(['K2captcha.title' => "This is a human detection control. This is necessary to ensure that the person doing this action is not an automated script / robot."]);

        //inputName
        session(['K2captcha.inputName' => chr(rand(97,122)).md5(rand(0,999999))]);

        //Real value
        $var = $arrParams["base"] . " " . $arrParams["operations"][$arrParams["operationChoice"]] . " " . $arrParams["toOp"];
        $evar = eval("return $var;");
        session(['K2captcha.realValue' => $evar]);

        //Fake value
        $arrParams = self::createWithNumbersFakeValue($arrParams);

        //Available options
        $arrParams = self::createWithNumbersAvailableOptions($arrParams);


        return self::fillObjectFromSession();
    }

    private static function createWithNumbersAvailableOptions($arrParams)
    {
        $x = 0;
        $arrParams["toOpFake"] = rand($arrParams["min"], $arrParams["max"]);
        $arrParams["toOpFets"][] = $arrParams["toOpFake"];
        $numOptions = rand(($arrParams["min"] + 1), ($arrParams["max"] - 1)); //Les opcions son menys que el max
        while (count($arrParams["toOpFets"]) - 1 < $numOptions && $x < ($arrParams["max"] * 2)) { // mentres sigui menor al doble de intents deixem continuar
            $arrParams["toOpFake"] = rand(1, 10);
            if (!in_array($arrParams["toOpFake"], $arrParams["toOpFets"])) {
                $arrParams["toOpFets"][] = $arrParams["toOpFake"];
            }
            $x++;
        }
        if (!in_array($arrParams["toOp"], $arrParams["toOpFets"])) {
            $arrParams["toOpFets"][] = $arrParams["toOp"];
            shuffle($arrParams["toOpFets"]); //Barrejem pk no quedi al final
        }
        foreach ($arrParams["toOpFets"] as $arrParams["toOp"]) {
            $var = $arrParams["base"] . " " . $arrParams["operations"][$arrParams["operationChoice"]] . " " . $arrParams["toOp"];
            $evar = eval("return $var;");
            $evars[] = $evar;
        }
        session(['K2captcha.availableOptions' => $evars]);
        return $arrParams;
    }
    private static function createWithNumbersFakeValue($arrParams)
    {
        $arrParams["toOpFake"] = rand($arrParams["min"], $arrParams["max"]);
        $x = 0;
        while ($arrParams["toOpFake"] === $arrParams["toOp"] || $x < ($arrParams["max"] * 2)) { //mentres el numero sigui igual al original i no s'excedeixi del doble de intents, bussquem un fake
            $arrParams["toOpFake"] = rand($arrParams["min"], $arrParams["max"]);
            $x++;
        }
        $var = $arrParams["base"] . " " . $arrParams["operations"][$arrParams["operationChoice"]] . " " . $arrParams["toOpFake"];
        $evar = eval("return $var;");
        session(['K2captcha.fakeValue' => $evar]);

        return $arrParams;
    }
    private static function createWithNumbersQuestion($arrParams)
    {
        //Question
        $question = "[" . $arrParams["base"] . " " . $arrParams["operations"][$arrParams["operationChoice"]] . " " . $arrParams["toOp"] . "]";

        //Possibilitat de subtituir espais en blanc per un altre simbol
        $arrOpcionsBlanc[] = " ";
        $arrOpcionsBlanc[] = "  ";
        //$arrOpcionsBlanc[] = "`";
        //$arrOpcionsBlanc[] = "´";
        //$arrOpcionsBlanc[] = "´";
        //$arrOpcionsBlanc[] = "·";
        //$arrOpcionsBlanc[] = "··";
        //$arrOpcionsBlanc[] = "_";
        $op = rand(0, count($arrOpcionsBlanc) - 1);
        $simbol = $arrOpcionsBlanc[$op];
        $question = str_ireplace(" ", $simbol, $question);


        //Possibilitat de substituir claus [] per {} o ()
        $arrOpcionsClaus[] = "[";
        $arrOpcionsClaus[] = "[[";
        $arrOpcionsClaus[] = "[_[";
        $arrOpcionsClaus[] = "{";
        $arrOpcionsClaus[] = "{{";
        $arrOpcionsClaus[] = "{_{";
        $arrOpcionsClaus[] = "(";
        $arrOpcionsClaus[] = "((";
        $arrOpcionsClaus[] = "(_(";
        $arrOpcionsClaus[] = "/";
        //tancar
        $arrOpcionsClausT[] = "]";
        $arrOpcionsClausT[] = "]]";
        $arrOpcionsClausT[] = "]_]";
        $arrOpcionsClausT[] = "}";
        $arrOpcionsClausT[] = "}}";
        $arrOpcionsClausT[] = "}_}";
        $arrOpcionsClausT[] = ")";
        $arrOpcionsClausT[] = "))";
        $arrOpcionsClausT[] = ")_)";
        $arrOpcionsClausT[] = "\\";
        $op = rand(0, count($arrOpcionsClaus) - 1);
        $simbol = $arrOpcionsClaus[$op];
        $question = str_ireplace("[", $simbol, $question);
        $simbol = $arrOpcionsClausT[$op];
        $question = str_ireplace("]", $simbol, $question);

        session(['K2captcha.question' => $question]);

        return $arrParams;
    }

    public static function createWithColors()
    {
        session(['K2captcha.type' => __FUNCTION__]);
        //Encara no esta programada
        return self::createWithNumbers(1, 10);
        //return self::fillObjectFromSession();
    }

    /**
     * Retorna l'objecte ple amb les dades de la sessió
     * @return App\Models\K2captcha;
     */
    public static function fillObjectFromSession()
    {
        $c = new K2captcha();
        $c->question = session('K2captcha.question');
        if (session('K2captcha.title') !== null) {
            $c->title = session('K2captcha.title');
        } else {
            //Setejat en el model
        }
        $c->type = session('K2captcha.type');
        $c->inputName = session('K2captcha.inputName');
        $c->realValue = session('K2captcha.realValue');
        $c->fakeValue = session('K2captcha.fakeValue');
        $c->availableOptions = session('K2captcha.availableOptions');
        return $c;
    }
    /**
     * Retorna guarda les dades de la sessió segons els valors de l'objecte
     *
     * @return App\Models\K2captcha;
     */
    public static function fillSessionFromObject(K2captcha $c)
    {
        session(['K2captcha.question' => $c->question]);
        session(['K2captcha.title' => $c->title]);
        session(['K2captcha.type' => $c->type]);
        session(['K2captcha.inputName' => $c->inputName]);
        session(['K2captcha.realValue' => $c->realValue]);
        session(['K2captcha.fakeValue' => $c->fakeValue]);
        session(['K2captcha.availableOptions' => $c->availableOptions]);
        return $c;
    }
    /**
     * Budia la sessió i retorna l'objecte buit.
     *
     * @return boolean;
     */
    public static function clearSession()
    {
        session(['K2captcha.question' => null]);
        session(['K2captcha.type' => null]);
        session(['K2captcha.inputName' => null]);
        session(['K2captcha.realValue' => null]);
        session(['K2captcha.fakeValue' => null]);
        session(['K2captcha.availableOptions' => null]);
        return true;
    }

    /**
     * Comprova el valor enviat pel form, i buida el captcha
     *
     * @return boolean
     */
    public function isValid($formValue = "")
    {
        $c = self::fillObjectFromSession();
        if ($c->realValue == $formValue) {
            return true;
        } else {
            return false;
        }
    }
}
