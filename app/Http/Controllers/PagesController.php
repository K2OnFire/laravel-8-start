<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
        return view("pages.index");
    }
    
    public function about(){
        $name = "John";
        $names =["Johan","Michael", "Ivan"];
        return view("pages.about")
        ->with("name",$name)
        ->with("names",$names);
    }
}
