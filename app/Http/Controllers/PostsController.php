<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function index()
    {
        //Non fluent
        //DB::select(['table' => 'posts', 'where'=> ['id' => 1]]);
        //Fluent
        //DB::table('posts')->where('id',1)->get();
        $posts = DB::select('SELECT * FROM posts WHERE id = ?', [3]);
        $posts = DB::select('SELECT * FROM posts WHERE id = :id', ['id' => 2]);
        $posts = DB::table('posts') //possible posar comentaris en cada chain method
            //->select(['id', 'title', 'body']) //Camps a seleccionar, tot o alguna columna en concret
            //->distinct(); //Es combina amb el select() (crea select distinct)
            //->where('id', 1) //simple
            //->where('created_at','<',now()->subDay()) //comparatiu
            //->orWhere('id',2)
            //->orWhereBetween('id',[3,8]) //3 i 8 estan inclosos tb
            //->orWhereNotNull('id')
            //->whereRaw('id = ?',1)//SQL no escapat, es perillos
            //->whereRaw('id = 2')//SQL no escapat, es perillos
            //->whereRaw('id = :id',['id' => 3])//SQL no escapat, es perillos
            //->orderBy('title', 'asc')
            //->latest() //order by created_at DESC,
            //->oldest() //order by created_at ASC,
            //->inRandomOrder() //order by RAND,
            //->get("body"); //retornar-ho tot o el nom de la/es columna/es (si no s'especifica ->select(), sino el select te prioritat)
            // -- comandes de final de query:
            ->get();
            //->first(); //limit 1
            //->find(1); //Busca per id, limit1
            //->count();//Quants, similars: min(), max(), sum(), avg(),
            /* ->insert([
                'title'=> 'new title',
                'body' => 'new body'
            ]); //Retornarà true si ho ha aconseguit */
            /* ->update([
                'title'=> 'new title',
                'body' => 'new body update'
            ]); //Retornarà 1 si ho ha aconseguit, 0 si no ha fet canvis, numeros diferents si l'ha liat amb un update massiu, si no e spossa where farà un update massiu! */
            //->delete(); //Combinar amb el where, o els borrarà tots retornant respostes similars al update!
        dd($posts);
        return view("posts/index");
    }
}
