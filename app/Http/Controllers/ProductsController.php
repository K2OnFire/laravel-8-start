<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(){
        return view('products.index');
    }

    public function indexVar(){
        $title= "Això és un títol";
        $description= "Això és una descripció";
        $data = "";
        // $data = [
        //     'ProductOne' => 'iPhone',
        //     'ProdcutTwo' => 'Samsung'
        // ];

        //Compact method
        // return view('products.index', compact("title","description",'data'));

        //With method
        return view('products.index')
            ->with("title",$title)
            ->with("description",$description)
            ->with("data",$data);
    }

    public function showById($id){
        $data = [
            'method' => __METHOD__
        ];
        return "ID: ".$id;
        //Si no te viewer, laravel no es queixa, retorna text pla.
    }

    public function showByName($name){
        $products = [
            "iphone" => "iPhone",
            "samsung" => "samsung"
        ];
        
        if (!isset($products[$name])){
            $product = "Product '$name' is not available yet (1)";
        }else{
            $product = $products[$name];
        }
        //or
        $product = $products[$name] ?? "Product '$name' is not available yet (2)";
        $allData = [
            'class' => __CLASS__,
            'method' => __METHOD__,
            'line' => __LINE__,
            'name' => $name,
            'product' => $product 
        ];
        return view("products.product")
            ->with("allData",$allData);
        //Si no te viewer, laravel no es queixa, retorna text pla.
    }

    public function showByNameAndId($name,$id){
        $products = [
            "iphone" => "iPhone",
            "samsung" => "samsung"
        ];
        $product = $products[$name] ?? "Product '$name' is not available yet (2)";
        $allData = [
            'env' => env("APP_ENV"),
            'class' => __CLASS__,
            'method' => __METHOD__,
            'line' => __LINE__,
            'name' => $name,
            'product' => $product,
            'id' => $id,
            'url' => route('linkTo_ProductsNameId', ['name' => $name, 'id' => $id])
            //Les URLS ja es passen muntades al view, això vol dir que si tenim una llista on hem de generar enllaços
            //Generem els enllaços en la propietat (o key del array) URL en el cntroler, i les iterem en el blade.
        ];
        return view("products.product")
            ->with("allData",$allData);
        //Si no te viewer, laravel no es queixa, retorna text pla.
    }

    public function about(){
        return __LINE__.": About us page";
    }
}
