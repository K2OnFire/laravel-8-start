<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
//Validador especific dr form
use Illuminate\Validation\Rule;
use App\Http\Controllers\K2captchaController;

class CreateCarValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; //false= error 403 de laravel (unauthori)
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:cars',
            'founded' => 'required|integer|min:0|max:' . date('Y') . '',
            //'description' => 'optional',
        ];
    }
}
