<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\ProductsController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\CarsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name("linkTo_Welcome");

// -- Laravel 8 (New, IDE friendly)
Route::get('/products', [ProductsController::class,'index'])->name("linkTo_Products");

// -- laravel 8 (also new)
//Route::get('/products','App\Http\Controllers\ProductsController@index');

// -- Before laravel 8 (not working anymore)
//Route::get('/products', 'ProductsController@index');

Route::get('/products/about', [ProductsController::class,'about'])->name("linkTo_ProductsAbout");
Route::get('/productsVar', [ProductsController::class,'indexVar'])->name("linkTo_ProductsVar");
Route::get('/productsVar/{name}/{id}', [ProductsController::class,'showByNameAndId'])->where([
    'name'=>'[a-zA-Z]+', //Only lletres
    'id'=>'[0-9]+', //Only numeros
])->name("linkTo_ProductsNameId"); //S'ha de conservar l'ordredels parametres en l'array de validació
Route::get('/productsVar/{id}', [ProductsController::class,'showById'])->where('id','[0-9]+'); //Only numeros
Route::get('/productsVar/{name}', [ProductsController::class,'showByName'])->where('name','[a-zA-Z]+');  //only lletres
Route::get('/productsVar/{name}', [ProductsController::class,'showByName'])->where('name','[a-zA-Z0-9]+');  //numeros i/o lletres
Route::get('/productsVar/{name}', [ProductsController::class,'showByName']); //qualsevol cosa

/*
| -- Proves també funcionals

//Route to View
Route::get('/', function () {
    //return env("CREATOR_NAME");
    return view('welcome');
});

//Route to users - string
Route::get('/users', function () {
    return "Welcome to the users page";
});

//Route to users - array(JSON)
Route::get('/users', function () {
    return ['php','html','laravel'];
});

//Route to users - JSON object
Route::get('/users', function () {
    return response()->json([
        'name'=> 'Ivan',
        'course' => 'Prova Laravel Begginer to advanced'
    ]);
});

//Route to users - function
Route::get('/users', function () {
    return redirect("/");
});
*/

Route::get('/pages', [PagesController::class,'index'])->name("linkTo_Pages");
Route::get('/pages/about', [PagesController::class,'about'])->name("linkTo_PagesAbout");

Route::get('/posts', [PostsController::class,'index'])->name("linkTo_Posts");


//Amb resource, tots els metodes del controller (nomes els creats amb php artisan make:controller --resource) són enrutats automaticament
Route::resource('/cars', CarsController::class);

/* 2022
Juny: del 23 al 26 -> Bidinguisen (holanda), Defqon1
Juliol: del 8 al 12 -> Valencia, Octopus
Agost: 19 al 22 Amsterdam, Decibel outdoor
*/

Auth::routes(); //PLUGUIN INTELLEPHENSA DISPARA ERROR aqui, PERÒ NO N'HI HA
//Aquesta comanda de dalt es molt similar a Route::resource(), s'encarrega de totes les rutes

Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

