<h1>Exemples de directives i loops. veure codi amb VStudio Code</h2>

<h2>Directives</h2>
@if (5 < 10)
    <p>5 is lower tahn 10</p>
@elseif (5==10)
    <p>5 is equal than 10</p>
@else
    <p>All conditions are wrong!</p>
@endif

@unless(empty($name))
    <p>Name is not empty (unless)</p>
@endunless

@if (!empty($name))
    <p>Name is not empty (if)</p>
@endif

@empty(!$name)
    <p>Name is not empty (!empty, best way)</p>
@endempty
{{-- Veure diferent estructura (mes completa) amb @isset i @unless junts en /products/index.blade --}}
@isset($varNoDefinida)
    <p> La var Està definida</p>
@endisset
@empty($varNoDefinida)
    <p>La var no està definida</p>
@endempty


@switch($name)
    @case('Dary')
        <p>Name es Dary (switch)</p>
    @break
    @case('Johna')
        <p>Name es John (switch)</p>
    @break
    @default
        <p>Name no match found: {{ $name }} (switch)</p>
@endswitch

<h2>Loops</h2>


@for ($i=0; $i<10; $i++)
    <br/>The number is {{ $i }} (for)
@endfor

<br/>
@foreach ($names as $item)
    <br/>The name is {{ $item}} (foreach)
@endforeach

<br/>
@forelse ($names as $item)
    <br/>The name is {{ $item}} (forelse)
@empty
    <br/>There are no names (forelse empty)
@endforelse

<br/>
{{ $i=0;}} {{-- Al setejar una variable aixi provoca un prnt en blade, s'hauria de declarar en el controller --}}
@while ($i<10)
    <br/>The number is {{ $i }} (while)
    {{ $i++;}}
@endwhile