@extends('layouts/cars/app')

@section('content')
    {{-- titol --}}
    <div class="m-auto w-4/8 py-24">
        <div class="text-center">
            <h1 class="text-5xl uppercase font-bold">Create car</h1>
        </div>
    </div>
    {{-- container body --}}
    <div class="flex justify-center pt-20">
        <form action="/cars" method="post" enctype="multipart/form-data">
            @csrf
            <div class="block">
                <div class='mb-10'>
                    <input type="file"
                        class="block shadow-5xl p-2 w-80 italic placeholder-gray-400"
                        name="image" placeholder="Image...">
                </div>

                <div class='mb-10'>
                    <input type="text"
                        class="block shadow-5xl p-2 w-80 italic placeholder-gray-400 @error('name') bg-red-300 @enderror"
                        name="name" value="{{ old('name') }}" placeholder="Brand name...">
                    @error('name')
                        <div class="w-4/8 m-auto text-red-500 text-sm">{{ $message }}</div>
                    @enderror
                </div>

                <div class='mb-10'>
                    <input type="text"
                        class="block shadow-5xl p-2 w-80 italic placeholder-gray-400 @error('founded') bg-red-300 @enderror"
                        name="founded" value="{{ old('founded') }}" placeholder="Founded...">
                    @error('founded')
                        <div class="w-4/8 m-auto text-red-500 text-sm">{{ $message }}</div>
                    @enderror
                </div>

                <div class='mb-10'>
                    <input type="text"
                        class="block shadow-5xl p-2 w-80 italic placeholder-gray-400 @error('description') bg-red-300 @enderror"
                        name="description" value="{{ old('description') }}" placeholder="Description...">
                    @error('description')
                        <div class="w-4/8 m-auto text-red-500 text-sm">{{ $message }}</div>
                    @enderror
                </div>

                <div class='mb-10' title="{{ $captcha->title }}">
                    <label for="{{ $captcha->inputName }}">{{ $captcha->question }}:</p>
                        <input type="number"
                            class="block p-2 w-80 shadow-5xl italic placeholder-gray-400 @error($captcha->inputName) bg-red-300 @enderror"
                            name="{{ $captcha->inputName }}" placeholder="{{ $captcha->fakeValue }}?" autocomplete=off>
                        @error($captcha->inputName)
                            <div class="w-4/8 m-auto text-red-500 text-sm">{{ $message }}</div>
                        @enderror
                </div>
                <button class="bg-green-500 block shadow-5xl mb-10 p-2 w-80 uppercase font-bold">Submit</button>
            </div>
        </form>

        @if ($errors->any())
            <div class="w-4/8 m-auto text-center">
                @foreach ($errors->all() as $error)
                    <li class="text-red-500 list-none">
                        {{ $error }}
                    </li>
                @endforeach
            </div>
        @endif
    </div>
@endsection
