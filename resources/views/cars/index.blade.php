@extends('layouts/cars/app')

@section('content')
    <div class="m-auto w4/5 py-24">
        <div class="text-center">
            <h1 class="text-5xl uppercase font-bold">Cars</h1>
        </div>

        <div class="pt-10">
            @if (Auth::user())
                <a href="{{ url('cars/create') }}" class="border-b-2 pb-2 border-dotted italic text-gray-500">
                    Add a new car &rarr;
                </a>
            @else
                <p class="border-b-2 pb-2 border-dotted italic text-gray-300">Add new car (registered users only)</p>
            @endif
        </div>


        <div class="w-5/6 py-10">
            {{-- $cars --}}
            @foreach ($cars as $car) {{-- tant si treballem amb collections (objects $car->id) com amb serialization (arrays $car['id']), es digual --}}
                <div class="m-auto">
                    @if (isset(Auth::user()->id) && Auth::user()->id == $car->user_id)
                        <div class="float-right">
                            <a href="cars/{{ $car->id }}/edit"
                                class="border-b-2 pb-2 border-dotted italic text-green-500">
                                Edit &rarr;
                            </a>
                            <form action="cars/{{ $car->id }}" method="POST" class='pt-3'>
                                @csrf
                                @method('DELETE')
                                <button type='submit' class="border-b-2 pb-2 border-dotted italic text-red-500">Delete
                                    &rarr;</button>
                            </form>
                        </div>
                    @endif
                    <span class="uppercase text-blue-500 font-bold text-xs italic">Founded {{ $car->founded }}</span>

                    <h2 class="text-gray-700 text-5xl hover:text-gray-500">
                        <a href="/cars/{{ $car->id }}">{{ $car->name }}</a>
                    </h2>

                    <p class="text-lg text-gray-700 py-6">
                        {{ $car->description }}
                    </p>
                    <hr class="mt-4 mb-8">
                </div>
            @endforeach
        </div>

        {{-- La paginació deLaravel la munta automaticament amb bootstrap 4 --}}
        {{ $cars->links() }}
    </div>
@endsection
