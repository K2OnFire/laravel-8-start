@extends('layouts/cars/app')

@section('content')
    <div class="m-auto w4/5 py-24">
        <div class="text-center">
            @if ($car->image_path !== null)
                <img src="{{ asset('cars/images/'.$car->image_path) }}" alt="" class="w-3/4 mb-8 shadow-xl">
            @endif
            <h1 class="text-5xl uppercase font-bold">Car {{ $car->name }}</h1>
        </div>


        <div class="py-10 text-center">
            <div class="m-auto">
                <span class="uppercase text-blue-500 font-bold text-xs italic">Founded {{ $car->founded }}</span>

                <p class="text-lg text-gray-700 py-6">
                    {{ $car->description }}
                </p>


                {{-- models --}}
                <hr class="mt-4 mb-8">
                <ul>
                    <p class="text-lg text-gray-700 py-3">
                        Models:
                        @forelse ( $car->carModels as $model)
                            <li class="inline italic text-gray-600 px-1 py-6">
                                {{ $model['model_name'] }}
                                {{-- no es fa servir l'obkect operator -> perque $car ja es un model, les multiple spropietats de dins són arrays. --}}
                            </li>
                        @empty
                            <p>
                                No models found
                            </p>
                        @endforelse
                    </p>
                </ul>


                {{-- Headquarters --}}
                <hr class="mt-4 mb-8">
                <ul>
                    <p class="text-lg text-gray-700 py-3">
                        Headquarter:
                        @if (isset($car->headquarter))
                            <li class="inline italic text-gray-600 px-1 py-6">
                                {{ $car->headquarter->headquarters }}. {{ $car->headquarter->country }}
                            </li>
                        @else
                            <p>
                                No headquarters found
                            </p>
                        @endif
                    </p>
                </ul>

                {{-- models + engines + productionDates --}}
                <hr class="mt-4 mb-8">
                <table class="table-auto">
                    <tr class="bg-blue-100">
                        <th class="w-1/4 border-4 border-gray-500">
                            Model
                        </th>
                        <th class="w-1/4 border-4 border-gray-500">
                            Engines
                        </th>
                        <th class="w-1/4 border-4 border-gray-500">
                            Production date
                        </th>
                    </tr>
                    @forelse ($car->carModels as $model)
                        <tr>
                            <td class="border-4 border-gray-500">
                                {{ $model->model_name }}
                            </td>
                            <td class="border-4 border-gray-500">
                                @forelse ($car->engines as $engine)
                                    @if ($model->id == $engine->model_id)
                                        {{ $engine->engine_name }}<br />
                                    @endif
                                @empty
                                    <p>No engines</p>
                                @endforelse
                            </td>
                            <td class="border-4 border-gray-500">
                                {{--  Tot i que la taula te fk a cars_models, en el Model de laravele stà especificat que estigui en cars_id, igualment el hasOne nomes retorna 1
                                    Si es vol aconseguri el resutat dejitjat hauria de ser amb un hasMany --}}
                                @if (isset($car->productionDate->created_at))
                                    {{ date('d-m-Y', strtotime($car->productionDate->created_at)) }}
                                @else
                                    no date
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan='3' class="border-4 border-gray-500">
                                No models
                            </td>
                        </tr>
                    @endforelse
                </table>


                {{-- car_products --}}
                <hr class="mt-4 mb-8">
                <p class="text-left">
                    Product types:
                    @forelse ($car->products as $product)
                        {{ $product->name }}
                        <br/>
                    @empty
                        Does not have car products.
                    @endforelse
                </p>

            </div>
        </div>
    </div>
@endsection
