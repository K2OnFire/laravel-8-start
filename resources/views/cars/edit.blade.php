@extends('layouts/cars/app')

@section('content')
    {{-- titol --}}
    <div class="m-auto w-4/8 py-24">
        <div class="text-center">
            <h1 class="text-5xl uppercase font-bold">Update car</h1>
        </div>
    </div>
    {{-- container body --}}
    <div class="flex justify-center pt-20">
        <form action="/cars/{{ $car->id }}" method="post">
            @csrf
            @method('PUT')
            <div class="block">
                <input type="text" class="block shadow-5xl mb-10 p-2 w-80 italic placeholder-gray-400" name="name" value="{{ $car->name }}"
                    placeholder="Brand name...">

                <input type="text" class="block shadow-5xl mb-10 p-2 w-80 italic placeholder-gray-400" name="founded" value="{{ $car->founded }}"
                    placeholder="Founded...">

                <input type="text" class="block shadow-5xl mb-10 p-2 w-80 italic placeholder-gray-400" name="description" value="{{ $car->description }}"
                    placeholder="Description...">

                <p>{{ session()->get('K2captcha.question') }}:</p>
                <input type="number" class="block shadow-5xl mb-10 p-2 w-80 italic placeholder-gray-400" name="k2captcha"
                    placeholder="{{ session()->get('K2captcha.fakeValue') }}?" autocomplete=off>
                <button class="bg-green-500 block shadow-5xl mb-10 p-2 w-80 uppercase font-bold">Submit</button>
            </div>
        </form>
    </div>
@endsection
