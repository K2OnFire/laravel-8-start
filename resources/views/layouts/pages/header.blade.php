
<!-- Navigation -->
<nav class="top-menu-container">
    <div class="logo-header">
        <a href="">
            <img 
            src="https://cdn.pixabay.com/photo/2017/02/18/19/20/logo-2078018_960_720.png" 
            alt="Logo personal portfolio"
            title="Logo personal portfolio"
            style="width:20px;"
            />
        </a>
    </div>

    <ul>
        <li>
            {{-- request()->is('/')?'active':'' --}}
            <a href="/pages" class="{{ request()->is('pages') ? 'active' : '' }}">Home</a>
        </li>
        <li>
            <a href="/pages/about" class="{{ request()->is('pages/about') ? 'active' : '' }}">About</a>
        </li>
        <li>
            <a href="/pages/about" class="{{ request()->is('pages/about/*') ? 'active' : '' }}">About + apartats interns amb classe active</a>
        </li>
        <li>
            <a href="/pages/portfolio" class="{{ request()->is('pages/portfolio') ? 'active' : '' }}">Portfolio</a>
        </li>
        <li>
            <a href="/pages/contact" class="{{ request()->is('pages/contact') ? 'active' : '' }}">Contact</a>
        </li>
    </ul>
</nav>