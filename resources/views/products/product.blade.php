<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Products index</h1>
    <p>Titol: {{ $title ?? '' }}</p>
    <p>Descripció: {{ $description ?? '' }}</p>

    <h2>Iteració de $allData:</h2>
    <ul>
        @if (@isset($allData)) {{-- Si aquesta var esta definida/enviada al blade --}}
            @unless ($allData) {{-- Si aquesta bar existeix però està buida --}}
                <li>No hi han resultats per mostrar</li>
            @else
                @foreach ($allData as $key => $item)
                       <li><strong>{{ $key }}</strong>: {{ $item }}</li>
                @endforeach
            @endunless
        @else
            <li>{allData} no enviada</li>
        @endif
    </ul>

    <h2>Obtenció de rutes:</h2>
    <ul>
        <li> <strong>Route</strong>: {{ Request::path() ?? 'undefined' }} 
            <br/><small>(actual, desde view, '<strong>Request::path()</strong>')</small>
        </li>
        <li> <strong>Route</strong>: {{ route("linkTo_ProductsAbout") ?? "undefined" }} 
            <br/><small>(registrada en routes/web.php, no pot tenir parametres, '<strong>route("linkTo_ProductsAbout")</strong>')</small>
        </li>

        <li> <strong>Route</strong>: {{ $allData["url"] ?? 'undefined' }} 
            <br/><small>(actual, muntada al controller, '<strong>$allData["url"]</strong>')</small>
        </li>
    </ul>
    {{-- @route("linkTo_ProductsNameId"),['name'=>$allData['name'],'id'=>$allData['id']] --}}
    {{-- <li class="{{ Request::path() == 'about' ? 'active' : '' }}"><a href="/about">About</a></li>  --}}
</body>
</html>