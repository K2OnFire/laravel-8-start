<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Products index</h1>
    <p>Titol: {{ $title ?? '' }}</p>
    <p>Descripció: {{ $description?? '' }}</p>
    <h2>Products:</h2>
    @if (isset($data)) {{-- Si aquesta var esta definida/enviada al blade --}}
        @unless ($data) {{-- Si aquesta bar existeix però està buida --}}
            No hi han resultats per mostrar
        @else
            @foreach ($data as $key => $item)
                <p>
                    {{ $key }}: {{ $item }}
                </p>
            @endforeach
        @endunless
    @else
        {data} no enviada
    @endif
    <h2>Links viables:</h2>
    <ul>
        <li>
            <a href="{{ route("linkTo_Welcome") ?? "undefined" }}">
                linkTo_Welcome
            </a>
        </li>
        <li>
            <a href="{{ route("linkTo_Products") ?? "undefined" }}">
                linkTo_Products
            </a>
        </li>
        <li>
            <a href="{{ route("linkTo_ProductsAbout") ?? "undefined" }}">
                linkTo_ProductsAbout
            </a>
        </li>
        <li>
            <a href="{{ route("linkTo_ProductsVar") ?? "undefined" }}">
                linkTo_ProductsVar
            </a>
            <br/>/productsVar/{name[samsung/iphone]}/{id} 
            <br/>/productsVar/{name}
            <br/>/productsVar/{id}
        </li>
    </ul>
</body>
</html>